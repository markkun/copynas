# -*- coding: iso-8859-15
"""
    Llamada al programa
    copynas.py "%D" "%N" "%L" "%K" "%F" 

    %F - Nombre del archivo descargado (para torrent de un solo archivo)
    %D - Directorio donde los archivos son guardados
    %N - Título del torrent
    %P - Estado previo del torrent
    %L - Etiqueta
    %T - Rastreador
    %M - Mensaje de estado (igual que columna de estado)
    %I - Estructura de datos codificado en hex
    %S - Estado del torrent
    %K - Clase de torrent (simple|multi)

    Donde estado es uno de:

    Error - 1
    Comprobado - 2
    Pausado - 3
    Semilla inicial - 4
    Compartiendo - 5
    Descargando - 6
    Semilla inicial [F] - 7
    Compartiendo [F] - 8
    Descargando [F] - 9
    Semilla en cola - 10
    Finalizado - 11
    Cola - 12
    Detenido - 13


"""

import os, sys, re, shutil, series
from datetime import datetime

#Definimos como directorio de trabajo allí donde está el script
os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

# Ruta por defecto
defaultDirectory = "Z:\\Descargas\\"

# Etiquetas que no se copiarán
exceptionTags = ["EPUBLIBRE",""]

# Parámetros
directory = sys.argv[1]
title = sys.argv[2]
tag = sys.argv[3]
wtype = sys.argv[4]
filename = sys.argv[5]

destination = ""


def writeLog(text):
    print(text)
    l=open("log.txt","a")
    l.write("%s\n" % text)
    l.close()

if __name__ == '__main__':

    if tag not in exceptionTags:

        writeLog ("%s - CopyNAS iniciado." % str(datetime.now()))
        writeLog ("%s - Directorio: %s" % (str(datetime.now()),directory))
        writeLog ("%s - Titulo: %s" % (str(datetime.now()),title))
        writeLog ("%s - Etiqueta: %s" % (str(datetime.now()),tag))
        writeLog ("%s - Tipo: %s" % (str(datetime.now()),wtype))
        writeLog ("%s - Fiechero: %s" % (str(datetime.now()),filename))

        if len(sys.argv) >= 3:
           
            # Comprobamos si es una de las series catalogadas   
            for serie in series.listado:
                if re.match(serie[0],title):
                    destination = serie[1]

            if not destination:
                if tag:
                    destination = "%s%s\\" % (defaultDirectory, tag)
                else:
                    destination = defaultDirectory

            writeLog("%s - Copiando: \"%s\" a la ruta \"%s\"" % (str(datetime.now()),title,destination))

            try:
                if wtype == "single":

                    shutil.copyfile("%s\\%s" % (directory, filename), "%s%s" % (destination,filename))
                else:
                    shutil.copytree(directory, "%s%s" % (destination,title))

            except Exception, e:
                raise e
                writeLog("%s" % e.ToString())                
        else:
            writeLog ("%s - Faltan parámetros." % str(datetime.now()))

        writeLog ("%s - CopyNAS finalizado." % str(datetime.now()))